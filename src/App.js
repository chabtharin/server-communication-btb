import logo from './logo.svg';
import './App.css';
import { Route, Routes } from 'react-router-dom';
import HomeComponent from './pages/HomeComponent';
import NavBarComponent from './component/NavBarComponent';
import CreatePostComponent from './pages/CreatePostComponent';

function App() {
  return (
    <div className="App">
      <NavBarComponent/>
      <Routes>
        <Route path='/' element={<HomeComponent/>}/>
        <Route path='/create' element={<CreatePostComponent/>}/>
      </Routes>
    </div>
  );
}

export default App;
