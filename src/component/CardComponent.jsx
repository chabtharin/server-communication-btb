import React from "react";
import { Button, Card } from "react-bootstrap";

export default function CardComponent({item, handleDelete}) {
  return (
    <div>
      <Card>
        <Card.Img variant="top" src={item.image ?? "https://socialistmodernism.com/wp-content/uploads/2017/07/placeholder-image.png?w=640"} />
        <Card.Body>
          <Card.Title>{item.title}</Card.Title>
          <Card.Text>
            {item.description ?? "No info"}
          </Card.Text>
          <Button variant="primary">Go somewhere</Button>
          <Button variant="danger" onClick={() => handleDelete(item._id)}>delete</Button>
        </Card.Body>
      </Card>
    </div>
  );
}
