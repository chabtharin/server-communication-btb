import axios from "axios";
import React, { useEffect, useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import { api } from "../api/api";
import CardComponent from "../component/CardComponent";

export default function HomeComponent() {
  const [data, setData] = useState([]);
  useEffect(() => {
    api.get("/articles").then((r) => {
      setData(r.data.data);
      console.log(r);
    });
  }, []);

  const handleDelete = (id) => {
    const newData = data.filter(data => data._id !== id)
    setData(newData)
    api.delete(`/articles/${id}`).then((r) => {console.log(r.data.message);})
  }

  return (
    <div>
      <Container>
        <Row>
          {data.map((item, index) => (
            <Col xs={12} sm={6} md={4} key={index}>
              <CardComponent item={item} handleDelete={handleDelete} />
            </Col>
          ))}
        </Row>
      </Container>
    </div>
  );
}
